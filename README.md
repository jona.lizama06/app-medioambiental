### Proyecto Medio Ambiental

####Objetivo general:
Desarrollo de una aplicación móvil encargada de gestionar los servicios ofrecidos por la Dirección de Medio Ambiente, Aseo y Ornato de la Municipalidad de Lautaro, con la finalidad de optimizar procesos que actualmente se realizan de forma manual.

####Objetivos específicos:
- Analizar especificaciones y requerimientos funcionales y no funcionales para la elaboración de cada módulo.

- Diseñar y Desarrollar módulos y ventanas de registro de usuarios, extracción de residuos, ubicación de tolvas, puntos limpios móviles y clínica veterinaria.

- Testear el funcionamiento y navegación de los módulos asignados en el ambiente de producción.